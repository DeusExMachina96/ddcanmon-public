/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Marko
 */
public class Appointment {
    private String doctorName;
    private Date date;
    private String notes;

    public Appointment() {
    }

    public Appointment(String doctorName, Date date, String notes) {
        this.doctorName = doctorName;
        this.date = date;
        this.notes = notes;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDate() {
        DateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        return f.format(date);
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
    
    
}
