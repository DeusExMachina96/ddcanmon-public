/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import util.DB;

/**
 *
 * @author Marko
 */
@ManagedBean
@RequestScoped
public class Patient {

    @ManagedProperty("#{user}")
    private User user;
    
    private ArrayList<PatientData> patientData;
    private ArrayList<Appointment> upcomingAppointments;
    private ArrayList<Appointment> pastAppointments;
    
    /**
     * Creates a new instance of Patient
     */
    public Patient() {
    }

    public ArrayList<PatientData> getPatientData() {
        return patientData;
    }

    public void setPatientData(ArrayList<PatientData> patientData) {
        this.patientData = patientData;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<Appointment> getUpcomingAppointments() {
        return upcomingAppointments;
    }

    public void setUpcomingAppointments(ArrayList<Appointment> upcomingAppointments) {
        this.upcomingAppointments = upcomingAppointments;
    }

    public ArrayList<Appointment> getPastAppointments() {
        return pastAppointments;
    }

    public void setPastAppointments(ArrayList<Appointment> pastAppointments) {
        this.pastAppointments = pastAppointments;
    }
    
    
    @PostConstruct
    public void init() {
        
        patientData = new ArrayList<>();
        upcomingAppointments = new ArrayList<>();
        pastAppointments = new ArrayList<>();
        
        Connection conn = DB.getInstance().getConnection();
        try {
            Statement st = conn.createStatement();
            String patientDataQuery = 
                    "SELECT * "
                    + " FROM patient_data "
                    + " WHERE patient_id=" + user.getId()
                    + " ORDER BY measurement_date DESC";
            
            ResultSet rs = st.executeQuery(patientDataQuery);
            while(rs.next()) {
                PatientData pd = new PatientData();
                pd.setBpUpper(rs.getInt("bp_upper"));
                pd.setBpLower(rs.getInt("bp_lower"));
                pd.setHeartRateAvg(rs.getDouble("heart_rate_avg"));
                pd.setBodyTemp(rs.getDouble("body_temp"));
                pd.setOxygenLevel(rs.getDouble("oxygen_level"));
                pd.setDate(rs.getDate("measurement_date"));
                
                patientData.add(pd);
            }
            
            String patientAppointmentsQuery = 
                    " SELECT * "
                    + " FROM appointments A JOIN users U ON A.doctor_id=U.id "
                    + " WHERE patient_id=" + user.getId()
                    + " ORDER BY app_date";
            rs = st.executeQuery(patientAppointmentsQuery);
            Date today = new Date();
            while(rs.next()) {
                Appointment a = new Appointment();
                Date date = rs.getDate("app_date");
                a.setDoctorName(rs.getString("first_name") + " " + rs.getString("last_name"));
                a.setDate(date);
                a.setNotes(rs.getString("notes"));
                if(date.after(today)) {
                    upcomingAppointments.add(a);
                } else {
                    pastAppointments.add(a);
                }
                
            }
            st.close();
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
                DB.getInstance().putConnection(conn);
            } catch(Exception e) { 
                
            }
        }
    }
}
