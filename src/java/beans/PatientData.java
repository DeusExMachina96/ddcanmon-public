/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Marko
 */
public class PatientData {
    
    private Integer bpUpper;
    private Integer bpLower;
    private Double heartRateAvg;
    private Double bodyTemp;
    private Double oxygenLevel;
    private Date date;

    public PatientData() {
    }

    public PatientData(Integer bpUpper, Integer bpLower, Double heartRateAvg, Double bodyTemp, Double oxygenLevel, Date date) {
        this.bpUpper = bpUpper;
        this.bpLower = bpLower;
        this.heartRateAvg = heartRateAvg;
        this.bodyTemp = bodyTemp;
        this.oxygenLevel = oxygenLevel;
        this.date = date;
    }

    public String getDate() {
        DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        return f.format(date);
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public Integer getBpUpper() {
        return bpUpper;
    }

    public void setBpUpper(Integer bpUpper) {
        this.bpUpper = bpUpper;
    }

    public Integer getBpLower() {
        return bpLower;
    }

    public void setBpLower(Integer bpLower) {
        this.bpLower = bpLower;
    }

    public Double getHeartRateAvg() {
        return heartRateAvg;
    }

    public void setHeartRateAvg(Double heartRateAvg) {
        this.heartRateAvg = heartRateAvg;
    }

    public Double getBodyTemp() {
        return bodyTemp;
    }

    public void setBodyTemp(Double bodyTemp) {
        this.bodyTemp = bodyTemp;
    }

    public Double getOxygenLevel() {
        return oxygenLevel;
    }

    public void setOxygenLevel(Double oxygenLevel) {
        this.oxygenLevel = oxygenLevel;
    }
    
    
}
