/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import util.DB;

/**
 *
 * @author Marko
 */
@ManagedBean
@SessionScoped
public class User {

    private Integer id;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private Integer type;
    
    
    private String errorMessage;
    
    /**
     * Creates a new instance of User
     */
    public User() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getFullName() {
        return firstName + " " + lastName;
    }
    
    public String login() {
  
        Connection conn = DB.getInstance().getConnection();
        try {
            Statement st = conn.createStatement();
            String getUserInfoQuery = 
                    "SELECT * "
                    + " FROM users "
                    + " WHERE username=? AND password=?";
            PreparedStatement ps = conn.prepareStatement(getUserInfoQuery);
            ps.setString(1, username);
            ps.setString(2, password);
            
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()) {          
                firstName = rs.getString("first_name");
                lastName = rs.getString("last_name");
                type = rs.getInt("type");
                id = rs.getInt("id");
                
            } else {
                errorMessage = "Incorrect username or password";
                return "login-fail";
            }
            st.close();
        } catch (Exception e) {
            // Return error
        } finally {
            try {
                conn.close();
                DB.getInstance().putConnection(conn);
            } catch (Exception e) {
                
            }
        }
        
        errorMessage = "";
        return "login-success";
    }
    
    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "logout";
    }
    
    
}
