DROP DATABASE IF EXISTS ddcanmon;
CREATE DATABASE ddcanmon DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE ddcanmon;

CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    username varchar(16) NOT NULL,
    password varchar(255) NOT NULL,
    first_name varchar(255) NOT NULL,
    last_name varchar(255) NOT NULL,
    type tinyint(4) NOT NULL, -- 1 == doctor, 0 == patient
    PRIMARY KEY (id)
);

CREATE TABLE patient_data (
    patient_id INT REFERENCES users.id ON UPDATE CASCADE ON DELETE CASCADE,
    measurement_date date NOT NULL,
    bp_upper INT,
    bp_lower INT,
    heart_rate_avg FLOAT(5, 2),
    body_temp FLOAT(4, 2),
    oxygen_level FLOAT(5, 2)
);

CREATE TABLE appointments (
    patient_id INT REFERENCES users.id ON UPDATE CASCADE ON DELETE CASCADE,
    doctor_id INT REFERENCES users.id ON UPDATE CASCADE ON DELETE CASCADE,
    app_date datetime NOT NULL,
    notes VARCHAR(1000)
);


INSERT INTO users(username, password, first_name, last_name, type) VALUES (
    'admin',
    'admin',
    'John',
    'Doe',
    0
);

INSERT INTO users(username, password, first_name, last_name, type) VALUES (
    'doc',
    'doc',
    'Jonathan',
    'Black',
    1
);


-- GENERATED DATA FOR patient_id = 1 --
INSERT INTO patient_data VALUES(1, '2018-05-18', 134, 69, 77.83, 36.18, 97.71);
INSERT INTO patient_data VALUES(1, '2018-05-19', 128, 59, 76.33, 36.52, 93.86);
INSERT INTO patient_data VALUES(1, '2018-05-20', 134, 70, 77.44, 36.12, 97.08);
INSERT INTO patient_data VALUES(1, '2018-05-21', 133, 71, 77.93, 36.38, 95.76);
INSERT INTO patient_data VALUES(1, '2018-05-22', 111, 63, 83.12, 36.14, 95.88);
INSERT INTO patient_data VALUES(1, '2018-05-23', 116, 83, 82.31, 36.06, 92.26);
INSERT INTO patient_data VALUES(1, '2018-05-24', 141, 86, 83.6, 36.65, 97.6);
INSERT INTO patient_data VALUES(1, '2018-05-25', 126, 75, 77.91, 36.03, 92.83);
INSERT INTO patient_data VALUES(1, '2018-05-26', 116, 82, 76.5, 36.01, 96.02);
INSERT INTO patient_data VALUES(1, '2018-05-27', 138, 83, 78.59, 36.3, 97.59);
INSERT INTO patient_data VALUES(1, '2018-05-28', 120, 73, 83.18, 36.03, 95.69);
INSERT INTO patient_data VALUES(1, '2018-05-29', 106, 65, 80.08, 36.16, 95.89);
INSERT INTO patient_data VALUES(1, '2018-05-30', 124, 72, 75.5, 36.0, 92.96);
INSERT INTO patient_data VALUES(1, '2018-05-31', 124, 79, 77.06, 36.08, 94.89);
INSERT INTO patient_data VALUES(1, '2018-06-01', 125, 86, 76.61, 36.05, 92.56);
INSERT INTO patient_data VALUES(1, '2018-06-02', 123, 75, 82.25, 36.29, 96.21);
INSERT INTO patient_data VALUES(1, '2018-06-03', 119, 74, 80.15, 36.01, 96.91);
INSERT INTO patient_data VALUES(1, '2018-06-04', 126, 85, 78.48, 36.23, 93.5);
INSERT INTO patient_data VALUES(1, '2018-06-05', 103, 69, 80.0, 36.18, 95.46);
INSERT INTO patient_data VALUES(1, '2018-06-06', 98, 60, 74.11, 36.13, 93.19);
INSERT INTO patient_data VALUES(1, '2018-06-07', 128, 59, 76.0, 36.23, 94.25);
INSERT INTO patient_data VALUES(1, '2018-06-08', 122, 72, 83.35, 36.3, 98.5);
INSERT INTO patient_data VALUES(1, '2018-06-09', 121, 84, 76.19, 36.15, 95.71);
INSERT INTO patient_data VALUES(1, '2018-06-10', 143, 92, 81.51, 36.09, 97.79);
INSERT INTO patient_data VALUES(1, '2018-06-11', 115, 67, 79.77, 36.34, 93.45);
INSERT INTO patient_data VALUES(1, '2018-06-12', 142, 82, 81.18, 36.36, 95.53);
INSERT INTO patient_data VALUES(1, '2018-06-13', 117, 75, 74.52, 36.24, 94.03);
INSERT INTO patient_data VALUES(1, '2018-06-14', 130, 75, 87.48, 36.05, 95.24);
INSERT INTO patient_data VALUES(1, '2018-06-15', 115, 75, 82.36, 36.01, 96.39);
INSERT INTO patient_data VALUES(1, '2018-06-16', 149, 96, 80.6, 36.77, 95.73);
INSERT INTO patient_data VALUES(1, '2018-06-17', 129, 69, 68.75, 36.26, 91.93);
INSERT INTO patient_data VALUES(1, '2018-06-18', 122, 74, 82.1, 36.22, 93.92);
INSERT INTO patient_data VALUES(1, '2018-06-19', 119, 80, 78.3, 36.16, 92.33);
INSERT INTO patient_data VALUES(1, '2018-06-20', 128, 79, 82.38, 36.02, 90.27);
INSERT INTO patient_data VALUES(1, '2018-06-21', 128, 55, 82.41, 36.07, 97.45);
INSERT INTO patient_data VALUES(1, '2018-06-22', 127, 94, 81.73, 36.2, 93.25);
INSERT INTO patient_data VALUES(1, '2018-06-23', 110, 76, 80.54, 36.24, 93.86);
INSERT INTO patient_data VALUES(1, '2018-06-24', 124, 87, 74.44, 36.12, 90.44);
INSERT INTO patient_data VALUES(1, '2018-06-25', 124, 75, 83.63, 36.44, 94.5);
INSERT INTO patient_data VALUES(1, '2018-06-26', 130, 82, 76.6, 36.12, 93.97);
INSERT INTO patient_data VALUES(1, '2018-06-27', 106, 65, 80.76, 36.22, 94.75);
INSERT INTO patient_data VALUES(1, '2018-06-28', 122, 80, 80.89, 36.2, 94.75);
INSERT INTO patient_data VALUES(1, '2018-06-29', 124, 80, 75.71, 36.43, 93.76);
INSERT INTO patient_data VALUES(1, '2018-06-30', 124, 72, 78.78, 36.32, 93.53);
INSERT INTO patient_data VALUES(1, '2018-07-01', 136, 90, 83.75, 36.09, 93.65);
INSERT INTO patient_data VALUES(1, '2018-07-02', 106, 71, 85.3, 36.48, 94.96);
INSERT INTO patient_data VALUES(1, '2018-07-03', 140, 81, 79.76, 36.1, 99);
INSERT INTO patient_data VALUES(1, '2018-07-04', 146, 87, 84.46, 36.62, 97.26);
INSERT INTO patient_data VALUES(1, '2018-07-05', 118, 72, 75.88, 36.05, 93.34);
INSERT INTO patient_data VALUES(1, '2018-07-06', 142, 84, 77.27, 36.13, 95.2);

INSERT INTO appointments VALUES(1, 2, '2018-05-23 13:00:00', 'Maintain a healthy weight. For many people, this means avoiding weight loss by getting enough calories every day. For people who are obese, this may mean losing weight. Ask your health care team if it is okay to try to lose weight during treatment. It may be better to wait until after treatment ends. If it is okay, weight loss should be moderate, meaning only about a pound a week.');
INSERT INTO appointments VALUES(1, 2, '2018-06-05 17:00:00', 'Data looks good. Regular checks every month.');
INSERT INTO appointments VALUES(1, 2, '2018-07-06 12:00:00', '');
INSERT INTO appointments VALUES(1, 2, '2018-08-06 12:00:00', '');
INSERT INTO appointments VALUES(1, 2, '2018-09-06 12:00:00', '');
